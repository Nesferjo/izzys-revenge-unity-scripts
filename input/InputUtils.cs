﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.scripts.input
{
    internal class InputUtils
    {
        public const float tapTimeWindow_sec = 0.05f;

        private static readonly Dictionary<InputValues, float> values = new Dictionary<InputValues, float>();


        public static InputResult readInput()
        {
            var result = new InputResult();


            result.set(InputValues.LEFT, Input.GetKey("a"));
            result.set(InputValues.RIGHT, Input.GetKey("d"));
            result.set(InputValues.DOWN, Input.GetKey("s"));
            result.set(InputValues.UP, Input.GetKey("w"));

            getInputPulse("space", InputValues.JUMP, result);
            getInputPulse("j", InputValues.ATTACK, result);
            getInputPulse("k", InputValues.KICK, result);

            return result;
        }

        private static void getInputPulse(string keycode, InputValues value, InputResult result)
        {
           
            if (Input.GetKey(keycode))
            {
                float currentTime = Time.realtimeSinceStartup;
                float lastTimePressed;
                values.TryGetValue(value, out lastTimePressed);
                values[value] = currentTime;   
                    if (currentTime - lastTimePressed > tapTimeWindow_sec)
                    {
                        result.set(value, Input.GetKey(keycode));
                    }
            }
        }
    }
}