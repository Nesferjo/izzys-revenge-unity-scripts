﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public  class LevelLadderUtils
    {


      public static bool canGoUpLadder(Bounds bounds)
      {
          return (closeToALadderTop(bounds) && closeToALadderMiddle(bounds));
      }

      public static bool canGoDownLadder(Bounds bounds)
      {
          return (closeToALadderBottom(bounds) && closeToALadderMiddle(bounds));
      }


      private static bool closeToALadderTop(Bounds bounds)
      {
          return Physics2D.Linecast(new Vector2(bounds.min.x, bounds.max.y), new Vector2(bounds.max.x, bounds.max.y), 1 << Layers.LADDERS).collider != null;
      }

      private static bool closeToALadderBottom(Bounds bounds)
      {
          return Physics2D.Linecast(new Vector2(bounds.min.x, bounds.min.y), new Vector2(bounds.max.x, bounds.min.y), 1 << Layers.LADDERS).collider != null;
      }

      private static bool closeToALadderMiddle(Bounds bounds)
      {
          return Physics2D.Linecast(new Vector2(bounds.min.x, bounds.center.y), new Vector2(bounds.max.x, bounds.center.y), 1 << Layers.LADDERS).collider != null;
      }



    }

