﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.physics
{
    class DamageCollisionUtils
    {

        public static CharacterControllerScript detectCollisionsWithIzzy(Bounds bounds)
        {
            Collider2D enemyCollider = Physics2D.OverlapArea(bounds.min, bounds.max, 1 << Layers.IZZY);
            if (enemyCollider == null) return null;
            return enemyCollider.gameObject.GetComponent<CharacterControllerScript>();
        }

        public static CharacterControllerScript[] detectAllCollisionsWithEnemies(Bounds bounds)
        {
            Collider2D[] enemyColliders = Physics2D.OverlapAreaAll(bounds.min, bounds.max, 1 << Layers.ENEMIES);
            if (enemyColliders == null) return null;
            CharacterControllerScript[] enemies = new CharacterControllerScript[enemyColliders.Length];
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = enemyColliders[i].gameObject.GetComponent<CharacterControllerScript>();
            }
            return enemies;
        }




    }
}
