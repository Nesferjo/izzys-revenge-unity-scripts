﻿using UnityEngine;

namespace Assets.scripts
{
    public class LevelCollisionUtils
    {


        //GLOBAL COLLISION CONSTANTS
        //-----------------------------
        public const float rayCastResolution = 0.1f;
        public const float rayCastLength = 0.4f;
        public const float xMarginCoefficient = 0.0f;
        public const float yMarginCoefficient = 0.2f;
        //------------------------------
        public const float floorWidth=0.01f;    
        //-----------------------------

        public static CollisionResult detectCollisions(Bounds bounds)
        {
            float xMargin = (bounds.max.x - bounds.min.x)*xMarginCoefficient;
            float yMargin = (bounds.max.y - bounds.min.y)*yMarginCoefficient;

            RaycastHit2D leftHit = checkVerticalRayCastCollision(bounds.min.x + rayCastLength/2, bounds.min.y + yMargin,
                bounds.max.y - yMargin, new Vector2(-1, 0), rayCastLength);
            RaycastHit2D rightHit = checkVerticalRayCastCollision(bounds.max.x - rayCastLength/2, bounds.min.y + yMargin,
                bounds.max.y - yMargin, new Vector2(1, 0), rayCastLength);

            RaycastHit2D bottomHit = checkHorizontalRayCastCollision(bounds.min.y + rayCastLength/2,
                bounds.min.x + xMargin, bounds.max.x - xMargin, new Vector2(0, -1), rayCastLength);
            RaycastHit2D topHit = checkHorizontalRayCastCollision(bounds.max.y - rayCastLength/2, bounds.min.x + xMargin,
                bounds.max.x - xMargin, new Vector2(0, 1), rayCastLength);


            leftHit.distance -= rayCastLength/2;
            rightHit.distance -= rayCastLength/2;
            topHit.distance = -topHit.distance + rayCastLength/2;
            bottomHit.distance = -bottomHit.distance + rayCastLength/2;

            return new CollisionResult(leftHit, rightHit, topHit, bottomHit);
        }


        private static RaycastHit2D checkVerticalRayCastCollision(float x, float startY, float endY, Vector2 orientation,
            float rayCastDistance)
        {
            RaycastHit2D candidateHit = Physics2D.Raycast(new Vector2(x, startY), orientation, rayCastDistance,1<<Layers.COLLISIONS);

            for (float y = startY + rayCastResolution; y < endY; y += rayCastResolution)
            {
                candidateHit = evaluateCandidate(candidateHit,
                    Physics2D.Raycast(new Vector2(x, y), orientation, rayCastDistance, 1 << Layers.COLLISIONS));
            }
            return candidateHit;
        }


        private static RaycastHit2D checkHorizontalRayCastCollision(float y, float startX, float endX,
            Vector2 orientation, float rayCastDistance)
        {
            RaycastHit2D candidateHit = Physics2D.Raycast(new Vector2(startX, y), orientation, rayCastDistance, 1 << Layers.COLLISIONS);

            for (float x = startX + rayCastResolution; x < endX; x += rayCastResolution)
            {
                candidateHit = evaluateCandidate(candidateHit,
                    Physics2D.Raycast(new Vector2(x, y), orientation, rayCastDistance, 1 << Layers.COLLISIONS));
            }
            return candidateHit;
        }

        private static RaycastHit2D evaluateCandidate(RaycastHit2D candidateHit, RaycastHit2D hit)
        {
            if (candidateHit.collider == null)
            {
                candidateHit = hit;
            }
            else
            {
                if (hit.collider != null && hit.distance < candidateHit.distance) candidateHit = hit;
            }
            return candidateHit;
        }
    }
}