using System;
using System.Runtime.Serialization;


public class CharacterPhysicsConfig
{

    public static readonly float STD_FALLING_ACCELERATION = 1.0f;

     private String physicsName;
     private float airControlFactor;
     private float fallingAcceleration;
     private float floorFriction;
     private float floorReducedFriction;
     private float runningAcceleration;
     private float speedFallingLimit;
     private float speedJumping;
     private float speedLadder;
     private float speedRunningLimit;
     private float knockbackSpeed;
     private float bigKnockbackSpeed;


    public CharacterPhysicsConfig(String physicsName,float speedRunningLimit, float speedJumping, float speedLadder, float speedFallingLimit,
        float fallingAcceleration, float floorFriction, float floorReducedFriction, float airControlFactor,
        float runningAcceleration, float knockbackSpeed, float bigKnockbackSpeed)
    {

        this.physicsName = physicsName;
        this.speedRunningLimit = speedRunningLimit;
        this.speedJumping = speedJumping;
        this.speedFallingLimit = speedFallingLimit;
        this.fallingAcceleration = fallingAcceleration;
        this.floorFriction = floorFriction;
        this.floorReducedFriction = floorReducedFriction;
        this.airControlFactor = airControlFactor;
        this.runningAcceleration = runningAcceleration;
        this.speedLadder = speedLadder;
        this.knockbackSpeed = knockbackSpeed;
        this.bigKnockbackSpeed = bigKnockbackSpeed;
    }

    public float SpeedRunningLimit
    {
        get { return speedRunningLimit; }
    }

    public float SpeedJumping
    {
        get { return speedJumping; }
    }

    public float SpeedFallingLimit
    {
        get { return speedFallingLimit; }
    }

    public float FallingAcceleration
    {
        get { return fallingAcceleration; }
    }

    public float FloorFriction
    {
        get { return floorFriction; }
    }

    public float FloorReducedFriction
    {
        get { return floorReducedFriction; }
    }

    public float AirControlFactor
    {
        get { return airControlFactor; }
    }

    public float RunningAcceleration
    {
        get { return runningAcceleration; }
    }

    public float SpeedLadder
    {
        get { return speedLadder; }
    }

    public string PhysicsName
    {
        get { return physicsName; }
    }

    public float KnockbackSpeed
    {
        get { return knockbackSpeed; }
    }

    public float BigKnockbackSpeed
    {
        get { return bigKnockbackSpeed; }
    }
}