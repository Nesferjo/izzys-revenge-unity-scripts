﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts
{
    public class CollisionResult
    {
        private RaycastHit2D leftCollision;
        private RaycastHit2D rightCollision;
        private RaycastHit2D topCollision;
        private RaycastHit2D bottomCollision;

        public CollisionResult(RaycastHit2D leftCollision, RaycastHit2D rightCollision, RaycastHit2D topCollision, RaycastHit2D bottomCollision)
        {
            this.leftCollision = leftCollision;
            this.rightCollision = rightCollision;
            this.topCollision = topCollision;
            this.bottomCollision = bottomCollision;
        }

  
        public RaycastHit2D LeftCollision
        {
            get { return leftCollision; }
        }

        public RaycastHit2D RightCollision
        {
            get { return rightCollision; }
        }

        public RaycastHit2D TopCollision
        {
            get { return topCollision; }
        }

        public RaycastHit2D BottomCollision
        {
            get { return bottomCollision; }
        }
    }

}
