﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.database
{
    public class WeaponryDataBase
    {
        private static Weaponry weaponry;


        public static Weapon getWeapon(String name)
        {

            if (weaponry == null)
                initialize();

            foreach (Weapon weapon in weaponry)
            {
                if (weapon.WeaponName.Equals(name)) return weapon;
            }
            throw new Exception("Weapon " + name + " not found");
        }

        private static void initialize()
        {

            String fileName = Path.Combine(Path.GetDirectoryName(Application.dataPath), "Weaponry.xml");

            var fs = new FileStream(fileName, FileMode.Open);

            XmlDocument doc = new XmlDocument();
            doc.Load(fs);

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/Weaponry/Weapon");

            weaponry = new Weaponry();

            foreach (XmlNode node in nodes)
            {

                Weapon weapon = new Weapon(
                    node.SelectSingleNode("weaponName").InnerText,
                    int.Parse(node.SelectSingleNode("damage").InnerText),
                    float.Parse(node.SelectSingleNode("chargingSec").InnerText),
                    float.Parse(node.SelectSingleNode("firingSec").InnerText),
                    float.Parse(node.SelectSingleNode("coolDownSec").InnerText),
                    float.Parse(node.SelectSingleNode("damageWidth").InnerText),
                    float.Parse(node.SelectSingleNode("damageHeight").InnerText),
                    bool.Parse(node.SelectSingleNode("chaineable").InnerText),
                    bool.Parse(node.SelectSingleNode("isAreaDamage").InnerText),
                    float.Parse(node.SelectSingleNode("damageOffsetX").InnerText),
                    float.Parse(node.SelectSingleNode("damageOffsetY").InnerText));

                weaponry.Add(weapon);
                fs.Close();

            }
        }
    }
}