﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.database
{

    public class CharacterMiscDatabase : List<CharacterMiscConfig>
    {

        private static CharacterMiscDatabase database;

        public static CharacterMiscConfig getMiscConfig(String name)
        {
            if (database == null)
                initialize();

            foreach (CharacterMiscConfig config in database)
            {
                if (config.Name.Equals(name))
                {
                    return config;
                }
            }
            throw new Exception("Weapon " + name + " not found");
        }

        private static void initialize()
        {

            String fileName = Path.Combine(Path.GetDirectoryName(Application.dataPath), "MiscDatabase.xml");
            
            var fs = new FileStream(fileName, FileMode.Open);

            XmlDocument doc = new XmlDocument();
            doc.Load(fs);

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/MiscDatabase/MiscConfig");

            database = new CharacterMiscDatabase();

            foreach (XmlNode node in nodes)
            {
                CharacterMiscConfig config = new CharacterMiscConfig(
                    node.SelectSingleNode("configName").InnerText,
                    int.Parse(node.SelectSingleNode("health").InnerText),
                    float.Parse(node.SelectSingleNode("stunnedTime").InnerText));
                database.Add(config);
                fs.Close();
            }
        }
    }
}