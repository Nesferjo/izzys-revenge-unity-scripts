﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.database
{

    public class PhysicsDatabase : List<CharacterPhysicsConfig>
    {

        private static PhysicsDatabase database;


        public static CharacterPhysicsConfig getPhysicsConfig(String name)
        {
            if (database == null)
                initialize();

            foreach (CharacterPhysicsConfig config in database)
            {
                if (config.PhysicsName.Equals(name))
                {
                    return config;
                }
            }
            throw new Exception("Physics config " + name + " not found");
        }

        private static void initialize()
        {


            String fileName = Path.Combine(Path.GetDirectoryName(Application.dataPath), "PhysicsDatabase.xml");
            
            var fs = new FileStream(fileName, FileMode.Open);

            XmlDocument doc = new XmlDocument();
            doc.Load(fs);

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/PhysicsDatabase/PhysicsConfig");

            database =new PhysicsDatabase();

            foreach (XmlNode node in nodes)
            {
                CharacterPhysicsConfig config = new CharacterPhysicsConfig(
                    node.SelectSingleNode("physicsName").InnerText,
                    float.Parse(node.SelectSingleNode("speedRunningLimit").InnerText),
                    float.Parse(node.SelectSingleNode("speedJumping").InnerText),
                    float.Parse(node.SelectSingleNode("speedLadder").InnerText),
                    float.Parse(node.SelectSingleNode("speedFallingLimit").InnerText),
                    float.Parse(node.SelectSingleNode("fallingAcceleration").InnerText),
                    float.Parse(node.SelectSingleNode("floorFriction").InnerText),
                    float.Parse(node.SelectSingleNode("floorReducedFriction").InnerText),
                    float.Parse(node.SelectSingleNode("airControlFactor").InnerText),
                    float.Parse(node.SelectSingleNode("runningAcceleration").InnerText),
                    float.Parse(node.SelectSingleNode("knockbackSpeed").InnerText),
                    float.Parse(node.SelectSingleNode("bigKnockbackSpeed").InnerText));
                database.Add(config);
                fs.Close();
            }
        }
    }
}