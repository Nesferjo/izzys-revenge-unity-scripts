﻿using System;
using UnityEngine;
using System.Collections;

public class LevelLadderScript : MonoBehaviour {


    

	// Use this for initialization
	void Start ()
	{

	    gameObject.layer = Layers.LADDERS;
        if (!this.collider2D is BoxCollider2D) throw new Exception("LadderCollisions should contain a box collider");
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnDrawGizmos()
    {


        Gizmos.color = Color.red;
        BoxCollider2D collider = (BoxCollider2D)this.collider2D;

        float lineSeparation = 0.02f;
        int amountX = 4;
        int amountY = 4;

        drawLines(amountX, amountY, new Vector2(collider.bounds.max.x, collider.bounds.min.y), lineSeparation, new Vector2(collider.bounds.max.x, collider.bounds.max.y));
        drawLines(amountX, amountY, new Vector2(collider.bounds.min.x, collider.bounds.min.y), lineSeparation, new Vector2(collider.bounds.min.x, collider.bounds.max.y));
        drawLines(amountX, amountY, new Vector2(collider.bounds.min.x, collider.bounds.min.y), lineSeparation, new Vector2(collider.bounds.max.x, collider.bounds.min.y));
        drawLines(amountX, amountY, new Vector2(collider.bounds.min.x, collider.bounds.max.y), lineSeparation, new Vector2(collider.bounds.max.x, collider.bounds.max.y));

        drawLines(amountX, amountY, new Vector2(collider.bounds.max.x, collider.bounds.min.y), lineSeparation, new Vector2(collider.bounds.min.x, collider.bounds.max.y));
        drawLines(amountX, amountY, new Vector2(collider.bounds.min.x, collider.bounds.min.y), lineSeparation, new Vector2(collider.bounds.max.x, collider.bounds.max.y));

    }


    private static void drawLines(int amountX, int amountY, Vector2 point1, float lineSeparation, Vector2 point2)
    {
        for (int x = -amountX / 2; x < amountX / 2; x++)
        {
            for (int y = -amountY / 2; y < amountY / 2; y++)
            {
                Gizmos.DrawLine(new Vector2(point1.x + lineSeparation * x, point1.y + lineSeparation * y),
                    new Vector2(point2.x + lineSeparation * x, point2.y + lineSeparation * y));
            }
        }
    }

}
