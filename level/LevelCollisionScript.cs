﻿using System;
using UnityEngine;
using System.Collections;

public class LevelCollisionScript : MonoBehaviour {


    

	// Use this for initialization
	void Start ()
	{

	    gameObject.layer = Layers.COLLISIONS;
        if (!this.collider2D is EdgeCollider2D) throw new Exception("LevelLadderScript should contain an edge collider");
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnDrawGizmos()
    {


        Gizmos.color = Color.yellow;
        EdgeCollider2D collider = (EdgeCollider2D)this.collider2D;

        
        Gizmos.color = Color.yellow;
       
        for (int i = 0; i < collider.pointCount-1; i++)
        {

            Vector2 point1 = getCollisionVertex(collider, i);
            Vector2 point2 = getCollisionVertex(collider, i + 1);


            float lineSeparation=0.02f;
            int amountX = 4;
            int amountY = 4;

            drawLines(amountX, amountY, point1, lineSeparation, point2);
            Gizmos.DrawSphere(getCollisionVertex(collider, i), 0.2f);
        }
            
        Gizmos.DrawSphere(getCollisionVertex(collider, collider.pointCount - 1), 0.2f); 

    }

    private static void drawLines(int amountX, int amountY, Vector2 point1, float lineSeparation, Vector2 point2)
    {
        for (int x = -amountX/2; x < amountX/2; x++)
        {
            for (int y = -amountY/2; y < amountY/2; y++)
            {
                Gizmos.DrawLine(new Vector2(point1.x + lineSeparation*x, point1.y + lineSeparation*y),
                    new Vector2(point2.x + lineSeparation*x, point2.y + lineSeparation*y));
            }
        }
    }

    private Vector2 getCollisionVertex(EdgeCollider2D collider, int i)
    {
        return new Vector2(collider.points[i].x + transform.position.x, collider.points[i].y + transform.position.y);
    }
}
