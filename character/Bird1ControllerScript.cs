﻿using Assets.scripts.character.vo;
using Assets.scripts.database;

namespace Assets.scripts.character
{
    internal class Bird1ControllerScript : CharacterControllerScript
    {
        protected override void initializePhysics()
        {

            base.initializePhysics();
            CharacterPhysicsConfig = PhysicsDatabase.getPhysicsConfig("bird");
            gameObject.layer = Layers.ENEMIES;

        }

        protected override CharacterInput getMovementInput()
        {
            //return new CharacterInputFromUser();
            return new VoidCharacterInput();
        }

        protected override void configureWeapons()
        {
            _characterData.Weapons = new Weapon[1];
            _characterData.Weapons[0] = WeaponryDataBase.getWeapon("attack1");
        }
    }
}