﻿using Assets.scripts.character.collaborators.attack;
using Assets.scripts.character.vo;
using Assets.scripts.damage;
using Assets.scripts.database;

public class IzzyControllerScript : CharacterControllerScript
{



    protected override CharacterInput getMovementInput()
    {
        return new CharacterInputFromUser();
    }

    protected override void initializePhysics()
    {
        gameObject.layer = Layers.IZZY;
        CharacterPhysicsConfig = PhysicsDatabase.getPhysicsConfig("default");
        _physicsCollaborator = new IzzyPhysicsCollaborator(_characterData, CharacterPhysicsConfig, CharacterMiscConfig, bounds, rigidbody2D);

    }

    protected override void initializeAttackCollaborator()
    {
        attackCollaborator = new IzzyIdleCollaborator(_characterData);
    }

    protected override void configureWeapons()
    {
        _characterData.Weapons=new Weapon[7];
        _characterData.Weapons[0] = WeaponryDataBase.getWeapon("attack1");
        _characterData.Weapons[1] = WeaponryDataBase.getWeapon("attack2");
        _characterData.Weapons[2] = WeaponryDataBase.getWeapon("attack3");
        _characterData.Weapons[3] = WeaponryDataBase.getWeapon("kick");
        _characterData.Weapons[4] = WeaponryDataBase.getWeapon("upperAttack");
        _characterData.Weapons[5] = WeaponryDataBase.getWeapon("downerAttack");
        _characterData.Weapons[6] = WeaponryDataBase.getWeapon("fallDamage");
    }


    protected override void addWeaponDamage(Weapon currentWeapon)
    {
        DamageUtils.izzyDealsDamage(this, _physicsCollaborator.Bounds, currentWeapon);
    }
}