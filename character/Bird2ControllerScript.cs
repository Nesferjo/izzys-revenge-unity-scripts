﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.scripts.character
{
    class Bird2ControllerScript : CharacterControllerScript
    {

        protected override void initializePhysics()
        {
            base.initializePhysics();
            gameObject.layer = Layers.ENEMIES;
        }

        protected override CharacterInput getMovementInput()
        {
            return new VoidCharacterInput();
        }

        protected override void configureWeapons()
        {
            throw new NotImplementedException();
        }
    }
}
