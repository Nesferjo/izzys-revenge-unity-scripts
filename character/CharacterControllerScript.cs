﻿using System;
using Assets.scripts;
using Assets.scripts.character;
using Assets.scripts.character.collaborators.attack;
using Assets.scripts.character.vo;
using Assets.scripts.damage;
using Assets.scripts.database;
using UnityEngine;

public abstract class CharacterControllerScript : MonoBehaviour
{
    protected readonly CharacterData _characterData = new CharacterData();
    protected CharacterPhysicsConfig CharacterPhysicsConfig;
    protected CharacterMiscConfig CharacterMiscConfig;
    protected Bounds bounds;
    protected CharacterInput input;

    protected BasePhysicsCollaborator _physicsCollaborator;
    protected AnimationCollaborator aniAnimationCollaborator;
    protected IAttackCollaborator attackCollaborator;
    protected DamageCollaborator damageCollaborator;


    // Use this for initialization
    private void Start()
    { 
        bounds = GetComponent<SpriteRenderer>().bounds;
        input = getMovementInput();
        initializePhysics();
        initializeMiscConfig();
        aniAnimationCollaborator = new AnimationCollaborator(_characterData,transform,GetComponent<Animator>());
        damageCollaborator = new DamageCollaborator(_characterData, CharacterPhysicsConfig, CharacterMiscConfig);
        initializeAttackCollaborator();
        configureWeapons();
    }

    protected virtual void initializePhysics()
    {
        gameObject.layer = Layers.NEUTRALOBJECTS;
        CharacterPhysicsConfig = PhysicsDatabase.getPhysicsConfig("default");
        _physicsCollaborator = new BasePhysicsCollaborator(_characterData, CharacterPhysicsConfig, CharacterMiscConfig, bounds, rigidbody2D);
    }

    protected virtual void initializeMiscConfig()
    {
        CharacterMiscConfig = CharacterMiscDatabase.getMiscConfig("default");
    }

    protected virtual void initializeAttackCollaborator()
    {
        attackCollaborator = new IdleCollaborator(_characterData);
    }

    protected abstract CharacterInput getMovementInput();

    public Boolean receiveDamage(DamageInfo damageInfo)
    {

        return damageCollaborator.tryToApplyDamage(damageInfo);
    }

    protected abstract void configureWeapons();


    private void FixedUpdate()
    {
        InputResult inputResult = input.getInput();
        CharacterState previousState = CharacterData.currentState(_characterData, CharacterMiscConfig);
        Weapon previousWeapon = _characterData.CurrentWeapon;

        damageCollaborator.process();
        _physicsCollaborator.performPhysics(inputResult);
        attackCollaborator = attackCollaborator.evaluate(inputResult);
        CharacterState currentState = CharacterData.currentState(_characterData, CharacterMiscConfig);
        Weapon currentWeapon = _characterData.CurrentWeapon;

        if (currentWeapon!=null && previousWeapon != currentWeapon)
        {
            addWeaponDamage(currentWeapon);
        }

        aniAnimationCollaborator.updateAnimation(previousState, previousWeapon, currentState, currentWeapon);
    }

    protected virtual void addWeaponDamage(Weapon currentWeapon)
    {
        throw new System.NotImplementedException();
    }

    public CharacterOrientation GetCharacterOrientation()
    {
        return _characterData.Orientation;
    }
}