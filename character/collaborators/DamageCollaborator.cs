﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.character.vo;
using Assets.scripts.damage;
using UnityEngine;

namespace Assets.scripts.character
{
   public class DamageCollaborator
    {

       private CharacterData _characterData;
       private CharacterMiscConfig _miscConfig;
       private CharacterPhysicsConfig _physicsConfig;
       private ReceivedDamageInfo damageInfo;

       public DamageCollaborator(CharacterData characterData,CharacterPhysicsConfig physicsConfig, CharacterMiscConfig miscConfig)
        {
           _characterData = characterData;
           _physicsConfig = physicsConfig;
           _miscConfig = miscConfig;
        }

        public bool tryToApplyDamage(DamageInfo damageInfo)
        {

            if (this.damageInfo != null) return false;
            this.damageInfo = new ReceivedDamageInfo(Time.realtimeSinceStartup,damageInfo);
            return true;

        }


        public void process()
        {
        
            if (_characterData.ReceivedDamageInfo != null &&
                Time.realtimeSinceStartup - _characterData.ReceivedDamageInfo.DamageTimeStamp > _miscConfig.StunnedTime)
            {
                _characterData.ReceivedDamageInfo = null;
            }

            if (this.damageInfo != null)
            {
                _characterData.ReceivedDamageInfo = damageInfo;
                this.damageInfo = null;

                if (_characterData.ReceivedDamageInfo.ReceivedDamage.SourceOrientation==CharacterOrientation.right)     
                    _characterData.Velocity = new Vector2(_physicsConfig.KnockbackSpeed, _physicsConfig.KnockbackSpeed);

                if (_characterData.ReceivedDamageInfo.ReceivedDamage.SourceOrientation == CharacterOrientation.left)
                    _characterData.Velocity = new Vector2(-_physicsConfig.KnockbackSpeed, _physicsConfig.KnockbackSpeed);

                if (_characterData.ReceivedDamageInfo.ReceivedDamage.SourceOrientation == CharacterOrientation.none)
                {
                    if (_characterData.CenterPosition.x >=
                        _characterData.ReceivedDamageInfo.ReceivedDamage.Bounds.center.x)
                    {
                        _characterData.Velocity = new Vector2(-_physicsConfig.KnockbackSpeed,
                            _physicsConfig.KnockbackSpeed);
                    }
                    else
                    {
                        _characterData.Velocity = new Vector2(-_physicsConfig.KnockbackSpeed,
                            _physicsConfig.KnockbackSpeed);
                    }
                }
            }
        }

    }
}
