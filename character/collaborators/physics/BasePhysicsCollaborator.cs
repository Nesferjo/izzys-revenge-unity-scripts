﻿using System;
using Assets.scripts;
using Assets.scripts.character.vo;
using UnityEngine;


    public class BasePhysicsCollaborator
    {
        protected readonly CharacterData _characterData;
        protected readonly CharacterPhysicsConfig CharacterPhysicsConfig;
        protected readonly CharacterMiscConfig characterMiscConfig;
        protected readonly Rigidbody2D rigidbody2D;
        protected Bounds bounds;


        public BasePhysicsCollaborator(CharacterData characterData, CharacterPhysicsConfig characterPhysicsConfig, CharacterMiscConfig characterMiscConfig, Bounds bounds,
            Rigidbody2D rigidbody2D)
        {
            _characterData = characterData;
            CharacterPhysicsConfig = characterPhysicsConfig;
            this.characterMiscConfig = characterMiscConfig;
            this.bounds = bounds;
            this.rigidbody2D = rigidbody2D;
        }


        public void performPhysics(InputResult result)
        {
            Vector2 velocity = _characterData.Velocity;
            CollisionResult collisions = LevelCollisionUtils.detectCollisions(bounds);
            float deltaTime = Time.deltaTime;


            updateCharacterData(collisions, result, velocity);

            if (_characterData.OnLadder)
            {
                velocity = new Vector2(0, 0);
                if (LevelLadderUtils.canGoUpLadder(bounds) && result.get(InputValues.UP))
                    velocity = new Vector2(0, CharacterPhysicsConfig.SpeedLadder);
                if (LevelLadderUtils.canGoDownLadder(bounds) && result.get(InputValues.DOWN))
                    velocity = new Vector2(0, -CharacterPhysicsConfig.SpeedLadder);
            }
            else
            {
                raiseCharacter(collisions);

               // Debug.Log("a "+_characterData.Weapons.Length+" "+velocity);

                velocity = settleCharacter(collisions, velocity);
                // Debug.Log("b " + _characterData.Weapons.Length + " " + velocity);
                velocity = evaluateVerticalVelocity(result, velocity, deltaTime, collisions);
              //  Debug.Log("c " + _characterData.Weapons.Length + " " + velocity);
                velocity = evaluateHorizontalVelocity(result, velocity, deltaTime, collisions);
               // Debug.Log("d " + _characterData.Weapons.Length + " " + velocity);
            }
            performTimeStep(velocity, deltaTime);
            _characterData.Velocity = velocity;
        }

        private void updateCharacterData(CollisionResult collisions, InputResult input, Vector2 velocity)
        {
            _characterData.TouchingFloor = (collisions.BottomCollision.collider &&
                                            collisions.BottomCollision.distance <= LevelCollisionUtils.floorWidth);
            if (_characterData.TouchingFloor) _characterData.Crouched = input.get(InputValues.DOWN);
            else _characterData.Crouched = false;

            if (velocity.x > 0) _characterData.Orientation = CharacterOrientation.right;
            if (velocity.x < 0) _characterData.Orientation = CharacterOrientation.left;
            _characterData.CenterPosition = bounds.center;


            if (_characterData.OnLadder && input.offLadderPressed())
            {
                _characterData.OnLadder = false;
            }

            if (!_characterData.OnLadder && LevelLadderUtils.canGoUpLadder(bounds) && _characterData.CurrentWeapon==null && input.get(InputValues.UP))
            {
                _characterData.OnLadder = true;
            }
            if (!_characterData.OnLadder && LevelLadderUtils.canGoDownLadder(bounds) && input.get(InputValues.DOWN))
            {
                _characterData.OnLadder = true;
            }
        }

        private void raiseCharacter(CollisionResult collisions)
        {
            if (collisions.BottomCollision.collider &&
                collisions.BottomCollision.distance < -LevelCollisionUtils.floorWidth/2)
            {
                Vector2 position = rigidbody2D.transform.position;
                position.y -= collisions.BottomCollision.distance;
                rigidbody2D.transform.position = position;
            }
        }

        private Vector2 settleCharacter(CollisionResult collisions, Vector2 velocity)
        {
            if (collisions.BottomCollision.collider &&
                CharacterData.currentState(_characterData, characterMiscConfig) != CharacterState.hitted &&
                collisions.BottomCollision.distance > LevelCollisionUtils.floorWidth/2 && velocity.y <= 0)
            {
                Vector2 position = rigidbody2D.transform.position;
                position.y -= collisions.BottomCollision.distance;
                rigidbody2D.transform.position = position;
                _characterData.TouchingFloor = true;
                velocity.y = 0;
            }
            return velocity;
        }

        private void performTimeStep(Vector2 velocity, float deltaTime)
        {
            Vector2 position = rigidbody2D.transform.position;
            position.x += velocity.x*deltaTime;
            position.y += velocity.y*deltaTime;
            rigidbody2D.transform.position = position;
            bounds.center = new Vector2(position.x, position.y + bounds.size.y/2);
        }

        protected virtual Vector2 evaluateHorizontalVelocity(InputResult input, Vector2 velocity, float deltaTime,
            CollisionResult collisions)
        {
            velocity = applyHorizontalSpeeds(input, velocity, deltaTime);
            velocity = applyLateralCollisions(velocity, collisions);
            return velocity;
        }

        protected Vector2 applyHorizontalSpeeds(InputResult input, Vector2 velocity, float deltaTime)
        {
            //direction keys released
            if (cannotMoveHorizontally(input))
            {
                //decreasing character speed due to floor friction
                if (velocity.x != 0)
                {
                    float friction = 0f;
                    if (_characterData.TouchingFloor) friction = CharacterPhysicsConfig.FloorFriction;
                    if (input.get(InputValues.DOWN) && _characterData.CurrentWeapon==null) friction = CharacterPhysicsConfig.FloorReducedFriction;
                    float decreasedSpeed = velocity.x - friction*deltaTime*Math.Sign(velocity.x);
                    if (Math.Sign(decreasedSpeed) != Math.Sign(velocity.x))

                        velocity.x = 0;
                    else velocity.x = decreasedSpeed;
                }
            }
                //direction keys pressed
            else
            {
                float directionControlFactor = 1;

                if (!_characterData.TouchingFloor)
                {
                    directionControlFactor = CharacterPhysicsConfig.AirControlFactor;
                }

                //speed increase due to input keys
                if (input.get(InputValues.LEFT))
                    velocity.x -= CharacterPhysicsConfig.RunningAcceleration*directionControlFactor*deltaTime;
                if (input.get(InputValues.RIGHT))
                    velocity.x += CharacterPhysicsConfig.RunningAcceleration*directionControlFactor*deltaTime;

                //running speed limit
                if (Math.Abs(velocity.x) > CharacterPhysicsConfig.SpeedRunningLimit)
                {
                    velocity.x = CharacterPhysicsConfig.SpeedRunningLimit*Math.Sign(velocity.x);
                }
            }
            return velocity;
        }

        protected virtual bool cannotMoveHorizontally(InputResult input)
        {
            return (!(input.get(InputValues.LEFT) ^ input.get(InputValues.RIGHT)) || _characterData.Crouched);
        }

        protected Vector2 applyLateralCollisions(Vector2 velocity, CollisionResult collisions)
        {
            if (collisions.LeftCollision.collider && collisions.LeftCollision.distance <= 0 && velocity.x < 0)
                velocity.x = 0;
            if (collisions.RightCollision.collider && collisions.RightCollision.distance <= 0 && velocity.x > 0)
                velocity.x = 0;
            return velocity;
        }

        private Vector2 evaluateVerticalVelocity(InputResult input, Vector2 velocity, float deltaTime,
            CollisionResult collisions)
        {
            //if touching floor
            if (_characterData.TouchingFloor)
            {
                //jumping
                if (velocity.y<0) velocity.y = 0;
                if (input.get(InputValues.JUMP) && !_characterData.handlingWeapon())
                {
                    velocity.y = CharacterPhysicsConfig.SpeedJumping;
                }
            }
                //not touching floor
            else
            {
                //gravity fall and fall speed limit
                velocity.y -= CharacterPhysicsConfig.FallingAcceleration*deltaTime;
                if (velocity.y < -CharacterPhysicsConfig.SpeedFallingLimit) velocity.y = -CharacterPhysicsConfig.SpeedFallingLimit;
            }

            //head collision
            if (collisions.TopCollision.collider && collisions.TopCollision.distance <= 0 && velocity.y > 0)
                velocity.y = 0;
            return velocity;
        }

        public Bounds Bounds
        {
            get { return bounds; }
        }
    }
