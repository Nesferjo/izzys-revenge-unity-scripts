﻿using System;
using Assets.scripts;
using Assets.scripts.character.vo;
using UnityEngine;


    public class IzzyPhysicsCollaborator : BasePhysicsCollaborator
    {
        private static readonly float ATTACK_IMPULSE=1.5f;


        public IzzyPhysicsCollaborator(CharacterData characterData, CharacterPhysicsConfig characterPhysicsConfig, CharacterMiscConfig characterMiscConfig, Bounds bounds,
            Rigidbody2D rigidbody2D)
            : base(characterData, characterPhysicsConfig, characterMiscConfig, bounds, rigidbody2D)
        {

        }

        protected override Vector2 evaluateHorizontalVelocity(InputResult input, Vector2 velocity, float deltaTime,
            CollisionResult collisions)
        {

            velocity = applyHorizontalSpeeds(input, velocity, deltaTime);

            if (_characterData.TouchingFloor && input.get(InputValues.ATTACK) && input.get(InputValues.LEFT)) velocity.x = velocity.x - ATTACK_IMPULSE;
            if (_characterData.TouchingFloor && input.get(InputValues.ATTACK) && input.get(InputValues.RIGHT)) velocity.x = velocity.x + ATTACK_IMPULSE;

            velocity = applyLateralCollisions(velocity, collisions);
            return velocity;
        }

        protected override bool cannotMoveHorizontally(InputResult input)
        {
            return base.cannotMoveHorizontally(input) || _characterData.handlingWeapon();
        }
    }
