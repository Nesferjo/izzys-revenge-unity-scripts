﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.character
{
   public class AnimationCollaborator
    {

        private CharacterData _characterData;
        private Transform transform;
        private Animator animator;

        public AnimationCollaborator(CharacterData characterData, Transform transform, Animator animator)
        {
            _characterData = characterData;
            this.transform = transform;
            this.animator = animator;
        }


        public void updateAnimation(CharacterState previousState, Weapon previousWeapon, CharacterState currentState, Weapon currentWeapon)
        {

            if (_characterData.Orientation==CharacterOrientation.left)
            {
                Vector3 theScale = transform.localScale;
                theScale.x = -1;
                transform.localScale = theScale;
            }
            else
            {
                Vector3 theScale = transform.localScale;
                theScale.x = 1;
                transform.localScale = theScale;
            }
      

            if (currentState != previousState || (previousWeapon!=null && !previousWeapon.Equals(currentWeapon)))
            {
                switch (currentState)
                {
                    case CharacterState.attack:
                        animator.Play(_characterData.CurrentWeapon.WeaponName);
                        break;
                    case CharacterState.charging:
                    case CharacterState.cooldown:
                        animator.Play(_characterData.CurrentWeapon.WeaponName + "_" + currentState);
                        break;
                    default:
                        animator.Play(currentState.ToString());
                        break;
                }
            }

            if (currentState == CharacterState.ladder && _characterData.Velocity.y <= 0)
            {
                if (_characterData.Velocity.y == 0) animator.speed = 0;
                if (_characterData.Velocity.y < 0) animator.speed = -1;
            }
            else
            {
                animator.speed = 1;
            }

        }

    }
}
