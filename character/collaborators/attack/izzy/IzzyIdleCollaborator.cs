﻿using Assets.scripts.character.vo;
using Assets.scripts.damage;
using Assets.scripts.database;

namespace Assets.scripts.character.collaborators.attack
{
    public class IzzyIdleCollaborator : IzzyBaseAttackCollaborator
    {
        private readonly CharacterData _characterData;


        public IzzyIdleCollaborator(CharacterData characterData)
        {
            _characterData = characterData;
            _characterData.CurrentWeapon = null;
            _characterData.Attacking = false;
        }


        public override IAttackCollaborator evaluate(InputResult result)
        {
            return IdleHandler(result);
        }

        private IAttackCollaborator IdleHandler(InputResult result)
        {
            Weapon selectedWeapon = extractAppropiateWeapon(_characterData, result);
            if (selectedWeapon != null)
            {
                _characterData.CurrentWeapon = selectedWeapon;
                return new IzzyFiringCollaborator(_characterData);
            }

            return this;
        }
    }
}