﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.character.collaborators.attack
{
    public class IzzyFiringCollaborator : IzzyBaseAttackCollaborator
    {

       private float timeStamp;
       private CharacterData _characterData;

       private Weapon nextWeapon;


       public IzzyFiringCollaborator(CharacterData characterData)
       {

           _characterData = characterData;
           this.timeStamp = Time.realtimeSinceStartup;
           _characterData.Attacking = true;


       }

       public override IAttackCollaborator evaluate(InputResult result)
       {

           if (nextWeapon == null && _characterData.CurrentWeapon.Chaineable && timeStamp + _characterData.CurrentWeapon.FiringSec - Time.realtimeSinceStartup < COMBO_TIME_WINDOW)
           {
               nextWeapon = extractAppropiateWeapon(_characterData,result);
           }

           if (Time.realtimeSinceStartup > timeStamp + _characterData.CurrentWeapon.FiringSec)
           {
               if (nextWeapon != null)
               {
                   _characterData.CurrentWeapon = nextWeapon;
                   return new IzzyFiringCollaborator(_characterData);
               }
                   return new IzzyIdleCollaborator(_characterData); 

           }
           return this;
       }
    }
}
