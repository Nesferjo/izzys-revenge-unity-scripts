﻿using Assets.scripts.character.vo;

namespace Assets.scripts.character.collaborators.attack
{
    public abstract class IzzyBaseAttackCollaborator : IAttackCollaborator
    {
        public static readonly float COMBO_TIME_WINDOW = 0.15f;

        public abstract IAttackCollaborator evaluate(InputResult result);

        protected Weapon extractAppropiateWeapon(CharacterData _characterData, InputResult input)
        {
            if (input.get(InputValues.KICK))
            {
                //kick weapon
                return _characterData.Weapons[3];
            }

            if (input.get(InputValues.ATTACK))
            {
                if (input.get(InputValues.UP) && _characterData.TouchingFloor)
                {
                    //upper attack
                    return _characterData.Weapons[4];
                }

                if (input.get(InputValues.DOWN) && !_characterData.TouchingFloor)
                {
                    //down attack
                    return _characterData.Weapons[5];
                }

                //incrementa el ataque, si estas al 3 vuelve al 1. en el aire solo hasta el 2

                if (_characterData.CurrentWeapon == null) return _characterData.Weapons[0];

                if (_characterData.CurrentWeapon.Equals(_characterData.Weapons[0]))
                    return _characterData.Weapons[1];

                if (_characterData.CurrentWeapon.Equals(_characterData.Weapons[1]))
                {
                    if (_characterData.TouchingFloor) return _characterData.Weapons[2];
                    return _characterData.Weapons[0];
                }

                return _characterData.Weapons[0];
            }

            if (_characterData.CurrentWeapon!=null && _characterData.CurrentWeapon.Equals(_characterData.Weapons[5]) && _characterData.TouchingFloor)
            {
                return _characterData.Weapons[6];
            }

            return null;
        }
    }
}