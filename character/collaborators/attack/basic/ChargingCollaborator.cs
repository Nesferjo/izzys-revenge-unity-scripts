﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.character.collaborators.attack
{
    class ChargingCollaborator : IAttackCollaborator
    {
        private float timeStamp;
        private CharacterData _characterData;

        public ChargingCollaborator(CharacterData characterData)
        {
            _characterData = characterData;
            this.timeStamp = Time.realtimeSinceStartup;
            GetValue();
        }

        private void GetValue()
        {
            _characterData.LoadingWeapon = true;
        }

        public IAttackCollaborator evaluate(InputResult result)
        {
                        if (Time.realtimeSinceStartup > timeStamp + _characterData.CurrentWeapon.ChargingSec)
            {
                _characterData.LoadingWeapon = false;
                return new FiringCollaborator(_characterData);
            }
            return this;
        }
    }
}
