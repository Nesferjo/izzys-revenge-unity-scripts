﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.character.collaborators.attack
{
   public class FiringCollaborator : IAttackCollaborator
    {

       private float timeStamp;
       private CharacterData _characterData;

       public FiringCollaborator(CharacterData characterData)
       {
           _characterData = characterData;
           this.timeStamp = Time.realtimeSinceStartup;
           _characterData.Attacking = true;
       }

       public IAttackCollaborator evaluate(InputResult result)
       {


           if (Time.realtimeSinceStartup > timeStamp + _characterData.CurrentWeapon.FiringSec)
           {
               _characterData.Attacking = false;
               timeStamp = Time.realtimeSinceStartup;
               if (_characterData.CurrentWeapon.CoolDownSec == 0)
                 return new IdleCollaborator(_characterData);
               else
               {
                   return new CoolingDownCollaborator(_characterData);
               }
           }
           return this;
       }
    }
}
