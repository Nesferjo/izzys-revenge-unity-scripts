﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.character.collaborators.attack
{
    public class IdleCollaborator : IAttackCollaborator
    {

        private CharacterData _characterData;


        public IdleCollaborator(CharacterData characterData)
        {
            _characterData = characterData;
            _characterData.CurrentWeapon = null;
        }


        public IAttackCollaborator evaluate(InputResult result)
        {
            return IdleHandler(result);
        }

        private IAttackCollaborator IdleHandler(InputResult result)
        {
            if (result.get(InputValues.ATTACK))
            {
                Weapon selectedWeapon = _characterData.Weapons[0];
                _characterData.CurrentWeapon = selectedWeapon;

                if (selectedWeapon.ChargingSec == 0)
                {
                    return new FiringCollaborator(_characterData);
                }
                else
                {
                    return new ChargingCollaborator(_characterData);
                }
            }
            return this;
        }

    }
}
