﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.character.collaborators.attack
{
    class CoolingDownCollaborator : IAttackCollaborator
    {

       private float timeStamp;
       private CharacterData _characterData;

       public CoolingDownCollaborator(CharacterData characterData)
       {
           _characterData = characterData;
           this.timeStamp = Time.realtimeSinceStartup;
       }

        public IAttackCollaborator evaluate(InputResult result)
        {
            if (Time.realtimeSinceStartup > timeStamp + _characterData.CurrentWeapon.CoolDownSec)
            {
               return new IdleCollaborator(_characterData);
            }
            return this;
        }
    }
}
