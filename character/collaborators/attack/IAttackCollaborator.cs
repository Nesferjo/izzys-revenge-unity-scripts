﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.scripts.character.collaborators.attack
{
   public interface IAttackCollaborator
    {

        IAttackCollaborator evaluate(InputResult result);


    }
}
