﻿using System;
using Assets.scripts;
using Assets.scripts.character.vo;
using Assets.scripts.damage;
using UnityEngine;

public class CharacterData
{
    private int health=1;
    private bool hitted;
    private bool leaving;
    private bool entering;
    private bool attacking;

    private bool onLadder;
    private bool touchingFloor;
    private bool crouched;
    private Vector2 velocity;
    private Vector2 centerPosition;
    private bool segada;
    private CharacterOrientation orientation;

    private Weapon[] weapons;
    private Weapon currentWeapon;
    private bool loadingWeapon;
    private bool weaponCoolingDown;


    private ReceivedDamageInfo _receivedDamageInfo;


    public Weapon[] Weapons
    {
        get { return weapons; }
        set { weapons = value; }
    }

    public ReceivedDamageInfo ReceivedDamageInfo
    {
        get { return _receivedDamageInfo; }
        set { _receivedDamageInfo = value; }
    }

    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    public bool Hitted
    {
        get { return hitted; }
        set { hitted = value; }
    }

    public bool Leaving
    {
        get { return leaving; }
        set { leaving = value; }
    }

    public bool Entering
    {
        get { return entering; }
        set { entering = value; }
    }

    public bool Attacking
    {
        get { return attacking; }
        set { attacking = value; }
    }

    public Weapon CurrentWeapon
    {
        get { return currentWeapon; }
        set { currentWeapon = value; }
    }

    public bool LoadingWeapon
    {
        get { return loadingWeapon; }
        set { loadingWeapon = value; }
    }

    public bool WeaponCoolingDown
    {
        get { return weaponCoolingDown; }
        set { weaponCoolingDown = value; }
    }

    public bool OnLadder
    {
        get { return onLadder; }
        set { onLadder = value; }
    }

    public bool TouchingFloor
    {
        get { return touchingFloor; }
        set { touchingFloor = value; }
    }

    public bool Crouched
    {
        get { return crouched; }
        set { crouched = value; }
    }

    public Vector2 Velocity
    {
        get { return velocity; }
        set { velocity = value; }
    }


    public Vector2 CenterPosition
    {
        get { return centerPosition; }
        set { centerPosition = value; }
    }

    public bool Segada
    {
        get { return segada; }
        set { segada = value; }
    }

    public CharacterOrientation Orientation
    {
        get { return orientation; }
        set { orientation = value; }
    }


    public bool handlingWeapon()
    {
        return Attacking || LoadingWeapon || WeaponCoolingDown;
    }


    public static CharacterState currentState(CharacterData data,CharacterMiscConfig miscConfig)
    {
        if (data.Health <= 0)
        {
            return CharacterState.dead;
        }

        /*
         if (just_fall) {
             just_fall = !animations.get(AnimationSet.LAND).non_repetitive_finished(tiempoms);
             if (attack_state == 0) {
                 return AnimationSet.LAND;
             }
         }
         */

        if (data.ReceivedDamageInfo != null)
            return CharacterState.hitted;

        if (data.Entering) return CharacterState.enter;
        if (data.Leaving) return CharacterState.idle;
        if (data.Hitted) return CharacterState.hitted;
        if (data.Attacking) return CharacterState.attack;


        if (data.LoadingWeapon) return CharacterState.charging;
        if (data.WeaponCoolingDown) return CharacterState.cooldown;

        if (data.OnLadder)
        {
            return CharacterState.ladder;
        }

        if (data.TouchingFloor && data.Velocity.y <= 0)
        {
            if (data.Crouched)
            {
                if (Math.Abs(data.Velocity.x) > 0) return CharacterState.segada;
                if (data.Segada) return CharacterState.segadaIdle;
                return CharacterState.crouch;
            }
            if (Math.Abs(data.Velocity.x) > 0) return CharacterState.walking;
            return CharacterState.idle;
        }
        if (data.Velocity.y <= 0)
        {
            return CharacterState.air;
        }
        return CharacterState.jump;
    }


}