using Assets.scripts.damage;

public class ReceivedDamageInfo
{
    private float damageTimeStamp;
    private DamageInfo receivedDamage;

    public ReceivedDamageInfo(float damageTimeStamp, DamageInfo receivedDamage)
    {
        this.damageTimeStamp = damageTimeStamp;
        this.receivedDamage = receivedDamage;
    }

    public float DamageTimeStamp
    {
        get { return damageTimeStamp; }
    }

    public DamageInfo ReceivedDamage
    {
        get { return receivedDamage; }
    }
}