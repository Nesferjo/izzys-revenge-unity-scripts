﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.scripts.character.vo
{
    public class CharacterMiscConfig
    {
        private string name;
        private int maxHealth;
        private float stunnedTime;

        public CharacterMiscConfig(string name, int maxHealth, float stunnedTime)
        {
            this.name = name;
            this.maxHealth = maxHealth;
            this.stunnedTime = stunnedTime;
        }


        public string Name
        {
            get { return name; }
        }

        public int MaxHealth
        {
            get { return maxHealth; }
        }

        public float StunnedTime
        {
            get { return stunnedTime; }
        }
    }
}
