﻿using System;
using UnityEngine;

namespace Assets.scripts.character.vo
{
  
    public class Weapon
    {
        private Boolean chaineable;
        private float chargingSec;
        private float coolDownSec;
        private int damage;
        private Boolean isAreaDamage;

        private float damageHeight;
        private float damageWidth;
        private float damageOffsetX;
        private float damageOffsetY;

        private float firingSec;
        private String weaponName;


        public Weapon(string weaponName, int damage, float chargingSec, float firingSec, float coolDownSec,
            float damageWidth, float damageHeight, bool chaineable, bool isAreaDamage, float damageOffsetX, float damageOffsetY)
        {
            this.weaponName = weaponName;
            this.damage = damage;
            this.chargingSec = chargingSec;
            this.firingSec = firingSec;
            this.coolDownSec = coolDownSec;
            this.damageWidth = damageWidth;
            this.damageHeight = damageHeight;
            this.chaineable = chaineable;
            this.isAreaDamage = isAreaDamage;
            this.damageOffsetX = damageOffsetX;
            this.damageOffsetY = damageOffsetY;
        }


        public int Damage
        {
            get { return damage; }
        }

        public float ChargingSec
        {
            get { return chargingSec; }
        }

        public float FiringSec
        {
            get { return firingSec; }
        }

        public float CoolDownSec
        {
            get { return coolDownSec; }
        }

        public Vector2 DamageArea
        {
            get { return new Vector2(damageWidth, damageHeight); }
        }

        public string WeaponName
        {
            get { return weaponName; }
        }

        public bool Chaineable
        {
            get { return chaineable; }
        }

        protected bool Equals(Weapon other)
        {
            return string.Equals(weaponName, other.weaponName);
        }

        public float DamageOffsetX
        {
            get { return damageOffsetX; }
        }

        public float DamageOffsetY
        {
            get { return damageOffsetY; }
        }

        public bool IsAreaDamage
        {
            get { return isAreaDamage; }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Weapon) obj);
        }

        public override int GetHashCode()
        {
            return (weaponName != null ? weaponName.GetHashCode() : 0);
        }
    }
}