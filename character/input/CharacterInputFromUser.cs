﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts;
using Assets.scripts.input;


public class CharacterInputFromUser : CharacterInput
    {
        public InputResult getInput()
        {
            return InputUtils.readInput();
        }
    }
