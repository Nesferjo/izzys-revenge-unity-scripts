﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Assets.scripts;
using UnityEngine;


public interface CharacterInput 
    {

     InputResult getInput();

    }

