﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.character.vo;
using Assets.scripts.database;

namespace Assets.scripts.character
{
    class PerroBotControllerScript : CharacterControllerScript
    {

        protected override void initializePhysics()
        {
            base.initializePhysics();
            gameObject.layer = Layers.ENEMIES;
        }

        protected override CharacterInput getMovementInput()
        {
            return new VoidCharacterInput();
        }

        protected override void configureWeapons()
        {
            _characterData.Weapons = new Weapon[1];
            _characterData.Weapons[0] = WeaponryDataBase.getWeapon("attack1");
        }
    }
}
