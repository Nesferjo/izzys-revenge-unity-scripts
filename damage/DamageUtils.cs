﻿using Assets.scripts.character.vo;
using UnityEngine;

namespace Assets.scripts.damage
{
    internal class DamageUtils
    {
        public static void izzyDealsDamage(IzzyControllerScript izzyScript, Bounds izzyBounds, Weapon weapon)
        {
            var damageObject = new GameObject();
            var script = damageObject.AddComponent<IzzyDamageScript>();

            Bounds bounds;

            if (izzyScript.GetCharacterOrientation() == CharacterOrientation.left)
            {
                bounds =
                    new Bounds(
                        new Vector2(izzyBounds.center.x - weapon.DamageOffsetX,
                            izzyBounds.center.y + weapon.DamageOffsetY), weapon.DamageArea);
            }

            else
            {
                bounds =
                    new Bounds(
                        new Vector2(izzyBounds.center.x + weapon.DamageOffsetX,
                            izzyBounds.center.y + weapon.DamageOffsetY), weapon.DamageArea);
            }


            script.initialize(izzyScript, new DamageInfo(bounds, weapon.Damage, izzyScript.GetCharacterOrientation()),
                false, Time.realtimeSinceStartup, 0.1f);
        }
    }
}