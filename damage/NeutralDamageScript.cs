﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.damage
{
    class NeutralDamageScript : MonoBehaviour
    {
        private Boolean initialized = false;
        private int damageToDeal;
        private Boolean areaDamage;
        private Time timeStamp;
        private float lifetime;


        public void initialize(int damageToDeal, bool areaDamage, Time timeStamp, float lifetime)
        {
            this.initialized = true;
            this.damageToDeal = damageToDeal;
            this.areaDamage = areaDamage;
            this.timeStamp = timeStamp;
            this.lifetime = lifetime;
        }
    }
}
