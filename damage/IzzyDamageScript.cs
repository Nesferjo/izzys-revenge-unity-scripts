﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Assets.scripts.physics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Assets.scripts.damage
{
    class IzzyDamageScript : MonoBehaviour
    {
        
        private Boolean areaDamage;
        private float timeStamp;
        private float lifetime;
        private IzzyControllerScript izzy;
        private DamageInfo _damageInfo;


        public void initialize(IzzyControllerScript izzy, DamageInfo damageInfo, bool areaDamage, float timeStamp, float lifetime)
        {
            this.izzy = izzy;
            this.areaDamage = areaDamage;
            this.timeStamp = timeStamp;
            this.lifetime = lifetime;
            this._damageInfo = damageInfo;
        }

        private void FixedUpdate()
        {

            CharacterControllerScript[] enemies = DamageCollisionUtils.detectAllCollisionsWithEnemies(_damageInfo.Bounds);

            bool applied = false;
            for (int i = 0; i < enemies.Length; i++)
            {
                applied |= enemies[i].receiveDamage(_damageInfo);
                if (!areaDamage && applied) break;
            }

            if (applied || Time.realtimeSinceStartup - timeStamp > lifetime)
            {
                Destroy(gameObject);
            }
        }



        private void OnGUI()
        {
            DrawQuad(_damageInfo.Bounds.min, _damageInfo.Bounds.max);
        }


        private void DrawQuad(Vector2 boxPosHiLeftWorld, Vector2 boxPosLowRightWorld)
        {
            Vector3 boxPosHiLeftCamera = Camera.main.WorldToScreenPoint(boxPosHiLeftWorld);
            Vector3 boxPosLowRightCamera = Camera.main.WorldToScreenPoint(boxPosLowRightWorld);

            float width = boxPosHiLeftCamera.x - boxPosLowRightCamera.x;
            float height = boxPosHiLeftCamera.y - boxPosLowRightCamera.y;


            GUI.Box(new Rect(boxPosHiLeftCamera.x - width, Screen.height - boxPosHiLeftCamera.y, width, height), "");

        }

    }
}
