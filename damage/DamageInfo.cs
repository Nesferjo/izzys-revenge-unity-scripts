using UnityEngine;

namespace Assets.scripts.damage
{
    public class DamageInfo
    {
        private Bounds bounds;
        private CharacterOrientation sourceOrientation;
        private int damageToDeal;


        public DamageInfo(Bounds bounds, int damageToDeal, CharacterOrientation sourceOrientation)
        {
            this.bounds = bounds;
            this.damageToDeal = damageToDeal;
            this.sourceOrientation = sourceOrientation;
        }

        public Bounds Bounds
        {
            get { return bounds; }
        }

        public int DamageToDeal
        {
            get { return damageToDeal; }
        }

        public CharacterOrientation SourceOrientation
        {
            get { return sourceOrientation; }
        }
    }
}