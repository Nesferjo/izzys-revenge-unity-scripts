﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CameraScript : MonoBehaviour
{

    public static readonly float MOVEMENT_COEFFICIENT=0.07f;

    public static readonly float Y_OFFSET = 2.2f;
    

    private Transform interestObject;

 

    private void Start()
    {
        interestObject = GameObject.Find("Izzy").transform;
        this.transform.position = new Vector3(interestObject.position.x, interestObject.position.y + Y_OFFSET, this.transform.position.z);
    }


    private void FixedUpdate()
    {
        Vector3 cameraPosition = this.transform.position;

        float xDifference= (interestObject.position.x-cameraPosition.x)*MOVEMENT_COEFFICIENT;
        float yDifference = (interestObject.position.y - cameraPosition.y + Y_OFFSET) * MOVEMENT_COEFFICIENT;

        this.transform.position = new Vector3(cameraPosition.x + xDifference, cameraPosition.y + yDifference, this.transform.position.z);
    }



}

